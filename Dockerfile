FROM openjdk:latest
COPY target/*.jar laboratory-4.jar
ENTRYPOINT ["java","-jar","/laboratory-4.jar"]