package ua.java.automation.laboratory4;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SimpleController {

    @GetMapping()
    public String helloWorld() {
        return "Hello world";
    }

    @GetMapping("/enhanced-greet")
    public String enhancedGreet(@RequestParam(name = "uppercase", defaultValue = "false") boolean isUppercase) {
        String greet = "Hello world";
        return isUppercase ? greet.toUpperCase() : greet;
    }

}
