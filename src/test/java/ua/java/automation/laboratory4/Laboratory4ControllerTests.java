package ua.java.automation.laboratory4;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class Laboratory4ControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testHelloWorld() throws Exception {
        mockMvc.perform(get("/")).andDo(print())
                .andExpect(content().string("Hello world"));
    }

    @Test
    public void enhancedGreetTest1() throws Exception {
        mockMvc.perform(get("/enhanced-greet")).andDo(print())
                .andExpect(content().string("Hello world"));
    }

    @Test
    public void enhancedGreetTest2() throws Exception {
        mockMvc.perform(get("/enhanced-greet?uppercase=false")).andDo(print())
                .andExpect(content().string("Hello world"));
    }

    @Test
    public void enhancedGreetTest3() throws Exception {
        mockMvc.perform(get("/enhanced-greet?uppercase=true")).andDo(print())
                .andExpect(content().string("HELLO WORLD"));
    }

}
