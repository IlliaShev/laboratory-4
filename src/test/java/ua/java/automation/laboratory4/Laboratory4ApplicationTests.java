package ua.java.automation.laboratory4;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
class Laboratory4ApplicationTests {

	@Autowired
	private SimpleController controller;

	@Test
	public void contextLoads() {
	}

	@Test
	public void controllerLoads() {
		assertThat(controller).isNotNull();
	}

}
